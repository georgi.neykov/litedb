package com.github.axet.litedb;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;
import com.almworks.sqlite4java.SQLiteQueue;
import com.almworks.sqlite4java.SQLiteStatement;

public abstract class SQLiteDAO<T extends SQLiteModel> {

    public static interface Listener<T extends SQLiteModel> {
        public void added(T item);

        public void removed(T item);

        public void changed(T old, T item);

        public void mark(T item);
    }

    protected SQLiteQueue db;

    protected List<String> columns;
    protected String table;
    protected List<Listener<T>> listeners = new ArrayList<Listener<T>>();

    public SQLiteDAO(SQLiteQueue db, String table, String[] columns) {
        this.columns = Arrays.asList(columns);
        this.table = table;
        this.db = db;
    }

    public void addListener(Listener<T> l) {
        listeners.add(l);
    }

    public void removeListener(Listener<T> l) {
        listeners.remove(l);
    }

    public abstract T get(SQLiteStatement st) throws SQLiteException;

    public abstract void bind(SQLiteStatement st, T f) throws SQLiteException;

    public String columns() {
        StringBuffer b = new StringBuffer();
        for (String s : columns) {
            b.append(s);
            b.append(",");
        }
        String str = b.toString();
        return StringUtils.removeEnd(str, ",");
    }

    public String columnUpdateMarks() {
        StringBuffer b = new StringBuffer();
        for (String s : columns) {
            b.append(s);
            b.append(" = ?,");
        }
        String str = b.toString();
        return StringUtils.removeEnd(str, ",");
    }

    public int columnIndex(String column) {
        for (int i = 0; i < columns.size(); i++) {
            if (columns.get(i).equals(column))
                return i;
        }

        throw new RuntimeException("column not found");
    }

    public int bindIndex(String column) {
        return columnIndex(column) + 1;
    }

    public String columnMarks() {
        StringBuffer b = new StringBuffer();
        for (int i = 0; i < columns.size(); i++) {
            b.append("? ,");
        }
        String str = b.toString();
        return StringUtils.removeEnd(str, ",");
    }

    public List<T> all(SQLiteConnection connection, Long offset, Long amount, String orderBy) throws SQLiteException {
        if (orderBy != null)
            orderBy = "order by " + orderBy;
        else
            orderBy = "";

        String limit = "";

        if (offset != null)
            limit = "limit ?, ?";
        else if (amount != null)
            limit = "limit ?";

        SQLiteStatement st = connection.prepare("select " + columns() + " from " + table + " " + orderBy + " " + limit);
        try {
            List<T> list = new ArrayList<T>();

            if (offset != null) {
                SQLiteUtils.bind(st, 1, offset);
                SQLiteUtils.bind(st, 2, amount);
            } else if (amount != null) {
                SQLiteUtils.bind(st, 1, amount);
            }

            while (st.step()) {
                list.add(get(st));
            }

            return list;
        } finally {
            st.dispose();
        }
    }

    public T get(SQLiteConnection connection, final Long folderId) throws SQLiteException {
        SQLiteStatement st = connection.prepare("select " + columns() + " from " + table + " where id is ?");
        try {
            SQLiteUtils.bind(st, 1, folderId);

            if (st.step()) {
                return get(st);
            }

            // not found
            return null;
        } finally {
            st.dispose();
        }
    }

    public T add(SQLiteConnection connection, final T f) throws SQLiteException {
        SQLiteStatement st = connection.prepare("insert into " + table + " (" + columns() + ")" + " values ("
                + columnMarks() + ")");
        try {
            bind(st, f);

            st.step();

            f.setId(connection.getLastInsertId());

            return f;
        } finally {
            st.dispose();
        }
    }

    public T update(SQLiteConnection connection, final T f) throws SQLiteException {
        SQLiteStatement st = connection.prepare("update " + table + " set " + columnUpdateMarks() + " where id = ?");
        try {
            bind(st, f);

            SQLiteUtils.bind(st, columns.size() + 1, f.getId());

            st.step();

            return f;
        } finally {
            st.dispose();
        }
    }

    public void remove(SQLiteConnection connection, final T f) throws SQLiteException {
        SQLiteStatement st = connection.prepare("delete from " + table + " where id is ?");
        try {
            SQLiteUtils.bind(st, 1, f.getId());

            st.step();
        } finally {
            st.dispose();
        }
    }

    public long count(SQLiteConnection connection) throws SQLiteException {
        SQLiteStatement st = connection.prepare("select count(*) from " + table);
        try {
            if (st.step()) {
                return st.columnLong(0);
            }

            return 0;
        } finally {
            st.dispose();
        }
    }

    //
    // public wrappers
    //

    public List<T> all() {
        return all(null);
    }

    public List<T> all(final String orderBy) {
        return SQLiteUtils.execute(db, new SQLiteJob<List<T>>() {
            @Override
            protected List<T> job(SQLiteConnection connection) throws Throwable {
                return all(connection, null, null, orderBy);
            }
        });
    }

    public List<T> all(final long offset, final long amount) {
        return SQLiteUtils.execute(db, new SQLiteJob<List<T>>() {
            @Override
            protected List<T> job(SQLiteConnection connection) throws Throwable {
                return all(connection, offset, amount, null);
            }
        });
    }

    public List<T> all(final long offset, final long amount, final String orderBy) {
        return SQLiteUtils.execute(db, new SQLiteJob<List<T>>() {
            @Override
            protected List<T> job(SQLiteConnection connection) throws Throwable {
                return all(connection, offset, amount, orderBy);
            }
        });
    }

    public T get(final Long id) {
        return SQLiteUtils.execute(db, new SQLiteJob<T>() {
            @Override
            protected T job(SQLiteConnection connection) throws Throwable {
                return SQLiteDAO.this.get(connection, id);
            }
        });
    }

    public T add(final T f) {
        SQLiteUtils.execute(db, new SQLiteJobNoResult() {
            @Override
            protected void jobNoResult(SQLiteConnection connection) throws Throwable {
                add(connection, f);
            }
        });

        for (Listener<T> l : new ArrayList<Listener<T>>(listeners)) {
            l.added(f);
        }

        return f;
    }

    public void remove(final T f) {
        SQLiteUtils.execute(db, new SQLiteJobNoResult() {
            @Override
            protected void jobNoResult(SQLiteConnection connection) throws Throwable {
                remove(connection, f);
            }
        });

        for (Listener<T> l : new ArrayList<Listener<T>>(listeners)) {
            l.removed(f);
        }
    }

    public void update(final T f) {
        T old = SQLiteUtils.execute(db, new SQLiteJob<T>() {
            @Override
            protected T job(SQLiteConnection connection) throws Throwable {
                T old = SQLiteDAO.this.get(connection, f.getId());

                update(connection, f);

                return old;
            }
        });

        f.setMark(false);

        for (Listener<T> l : new ArrayList<Listener<T>>(listeners)) {
            l.changed(old, f);
        }
    }

    /**
     * mark item changed. (if you change internal state, whcih you wish to save
     * later to db)
     * 
     * @param f
     */
    public void mark(final T f) {
        f.setMark(true);

        for (Listener<T> l : new ArrayList<Listener<T>>(listeners)) {
            l.mark(f);
        }
    }

    public long count() {
        return SQLiteUtils.execute(db, new SQLiteJob<Long>() {
            @Override
            protected Long job(SQLiteConnection connection) throws Throwable {
                return count(connection);
            }
        });
    }
}
