package com.github.axet.litedb;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.ExecutionException;

import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteJob;
import com.almworks.sqlite4java.SQLiteQueue;
import com.almworks.sqlite4java.SQLiteStatement;
import com.thoughtworks.xstream.XStream;

public class SQLiteUtils {

    //
    // column
    //

    static public Long columnLong(SQLiteStatement st, int index) {
        try {
            Object s = st.columnValue(index);

            if (s == null)
                return null;
            if (s instanceof Integer)
                return new Long((Integer) s);
            if (s instanceof Long)
                return (Long) s;

            throw new RuntimeException("wrong column type");
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public String columnString(SQLiteStatement st, int index) {
        try {
            Object s = st.columnValue(index);

            if (s == null)
                return null;

            return (String) s;
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public Integer columnInt(SQLiteStatement st, int index) {
        try {
            Object s = st.columnValue(index);

            if (s == null)
                return null;
            if (s instanceof Integer)
                return (Integer) s;

            throw new RuntimeException("wrong column type");
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public Date columnDate(SQLiteStatement st, int index) {
        try {
            Object s = st.columnValue(index);
            if (s == null)
                return null;
            // Integer can be ssigned for small number (0 for example)
            if(s instanceof Integer)
                return new Date((Integer)s);
            return new Date((Long) s);
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public File columnFile(SQLiteStatement st, int index) {
        try {
            Object s = st.columnValue(index);
            if (s == null)
                return null;
            return new File((String) s);
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public Boolean columnBoolean(SQLiteStatement st, int index) {
        try {
            Object s = st.columnValue(index);
            if (s == null)
                return null;
            return new Boolean(((Integer) s).intValue() == 1);
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public Throwable columnThrowable(SQLiteStatement st, int index) {
        try {
            String s = st.columnString(index);

            if (s == null)
                return null;

            try {
                XStream x = new XStream();
                Throwable e = (Throwable) x.fromXML(s);
                return e;
            } catch (Exception ee) {
                // uanble to restore exception from xml? throw new one with
                // message
                return new Throwable(s);
            }
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        }
    }

    static public URL columnURL(SQLiteStatement st, int index) {
        try {
            String s = st.columnString(index);
            if (s == null)
                return null;
            return new URL(s);
        } catch (SQLiteException e) {
            throw new RuntimeException(e);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    //
    // bind
    //

    static public  SQLiteStatement bind(SQLiteStatement st, int index, Throwable value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else {
            XStream s = new XStream();
            String xml = s.toXML(value);
            return st.bind(index, xml);
        }
    }

    static public SQLiteStatement bind(SQLiteStatement st, int index, URL value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else
            return st.bind(index, value.toExternalForm());
    }

    static public SQLiteStatement bind(SQLiteStatement st, int index, File value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else
            return st.bind(index, value.getPath());
    }

    static public SQLiteStatement bind(SQLiteStatement st, int index, String value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else
            return st.bind(index, value);
    }

    static public SQLiteStatement bind(SQLiteStatement st, int index, Date value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else
            return st.bind(index, value.getTime());
    }

    static public SQLiteStatement bind(SQLiteStatement st, int index, Long value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else
            return st.bind(index, value);
    }

    static public SQLiteStatement bind(SQLiteStatement st, int index, Boolean value) throws SQLiteException {
        if (value == null)
            return st.bindNull(index);
        else
            return st.bind(index, value ? 1 : 0);
    }

    //
    // execute
    //

    static public <T, J extends SQLiteJob<T>> T execute(SQLiteQueue db, J j) {
        try {
            return db.execute(j).get();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (ExecutionException e) {
            try {
                throw e.getCause();
            } catch (RuntimeException ee) {
                throw ee;
            } catch (Throwable ee) {
                throw new RuntimeException(ee);
            }
        }
    }
}
