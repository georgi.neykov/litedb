package com.github.axet.vlc;

import java.io.File;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteStatement;
import com.github.axet.litedb.SQLiteNatives;

public class TestSql {

    public static void main(String[] args) {
        try {
            SQLiteNatives.init();

            SQLiteConnection db = new SQLiteConnection(new File("database"));
            db.open(true);
            // load data, call it once
            //db.exec("create table orders (order_id int, quantity int)");
            //db.exec("insert into orders values (1, 5)");
            SQLiteStatement st = db.prepare("SELECT order_id FROM orders WHERE quantity >= ?");
            try {
                int minimumQuantity = 1;
                st.bind(1, minimumQuantity);
                while (st.step()) {
                    System.out.println(st.columnLong(0));
                }
            } finally {
                st.dispose();
            }
            db.dispose();
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
